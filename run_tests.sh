#!/bin/bash -x
test -t 1 && USE_TTY="-it"
# Make sure our tools are installed and paths set
cookiecutter --version || pip install cookiecutter
poetry --version || curl -sSL https://install.python-poetry.org | POETRY_VERSION="1.2.2" python3 -
export PATH="/root/.local/bin:$PATH"
# Start aborting on failed commands
set -e
old_workdir=$(pwd)
tmp_dir=$(mktemp -d "/tmp/cclibtestbuild.XXXXXXXX")
cd $tmp_dir
cookiecutter -f --no-input $old_workdir
cd python-nameless

# Init repo so it looks like a real project
git init
git checkout -b cctest
poetry lock
git add .

if [ $(uname -s ) == "Darwin" ]; then
  export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
else
  export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"
fi

# We need buildkit
export DOCKER_BUILDKIT=1
# the plain test entrypoint
docker build --progress plain --ssh default --target test -t cclibtestbuild:test_debian -f Dockerfile_debian .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` cclibtestbuild:test_debian
docker build --progress plain --ssh default --target test -t cclibtestbuild:test_alpine -f Dockerfile_alpine .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` cclibtestbuild:test_alpine
# Tox tests under docker
docker build --progress plain --ssh default --target tox -t cclibtestbuild:tox_alpine -f Dockerfile_alpine .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` cclibtestbuild:tox_alpine
docker build --progress plain --ssh default --target tox -t cclibtestbuild:tox_debian -f Dockerfile_debian .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` cclibtestbuild:tox_debian
# Docker devel shell
docker build --progress plain --ssh default --target devel_shell -t cclibtestbuild:devel_shell_alpine -f Dockerfile_alpine .
docker run $USE_TTY --rm -v `pwd`:/app cclibtestbuild:devel_shell_alpine -c "source /root/.zshrc && SKIP=check-executables-have-shebangs pre-commit run --all-files"
docker run $USE_TTY --rm -v `pwd`:/app cclibtestbuild:devel_shell_alpine -c "source /root/.zshrc && which git-up"
docker build --progress plain --ssh default --target devel_shell -t cclibtestbuild:devel_shell_debian -f Dockerfile_debian .
docker run $USE_TTY --rm -v `pwd`:/app cclibtestbuild:devel_shell_debian -c "source /root/.zshrc && SKIP=check-executables-have-shebangs pre-commit run --all-files"
docker run $USE_TTY --rm -v `pwd`:/app cclibtestbuild:devel_shell_debian -c "source /root/.zshrc && which git-up"
# Docker production image
docker build --progress plain --ssh default --target production -t cclibtestbuild:production_alpine -f Dockerfile_alpine .
docker run $USE_TTY --rm cclibtestbuild:production_alpine true
docker build --progress plain --ssh default --target production -t cclibtestbuild:production_debian -f Dockerfile_debian .
docker run $USE_TTY --rm cclibtestbuild:production_debian true

rm -rf $tmp_dir
