# python-lib_template

Bare bones Cookiecutter template for new Python 3.8 projects.

Sets up the basics for quality code (pylint, mypy, pytest with coverage,
pre-commit framework with a set of hooks)

## Usage

  1. Install [cookiecutter](https://pypi.org/project/cookiecutter/)
  2. `cookiecutter gl:advian-oss/python-lib_template`
  3. Fill in the forms
  4. Profit!

The newly generated directory structure has more information, remember init your repo first.

    cd python-mypackage
    git init
    git add .
    git commit -m 'Cookiecutter stubs'
